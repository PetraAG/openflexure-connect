stages:
  - analysis
  - build
  - deploy
  - release

# Re-usable block to install (and cache) openflexure-microscope-server static app
.node-install-template: &node-install
  before_script:
    - npm install
  cache:
    key: "${CI_COMMIT_REF_SLUG}"
    paths:
      - node_modules

# JavaScript linting with ESLint (via Vue CLI)
eslint:
  stage: analysis
  image: electronuserland/builder:wine

  <<: *node-install

  script:
    # Build JS application for production
    - npm run lint

  only:
    - branches
    - merge_requests
    - tags
    - web

# Electron app build
build:
  stage: build
  dependencies:
    - eslint
  image: electronuserland/builder:wine

  <<: *node-install

  script:
    # Build electron app
    - npm run electron:release

  artifacts:
    name: "dist_electron"
    expire_in: 1 week
    paths:
      - "./dist_electron/*.AppImage"
      - "./dist_electron/*.snap"
      - "./dist_electron/*.exe"
      - "./dist_electron/*.appx"
      - "./dist_electron/*.exe.blockmap"
      - "./dist_electron/*.yml"
      - "./dist_electron/*.yaml"

  only:
    - merge_requests
    - web
    - tags


# Deploy to builds.openflexure.org
deploy:
  stage: deploy
  dependencies:
    - build
  image: ubuntu:latest
  
  before_script:
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
    - eval $(ssh-agent -s)
    - ssh-add <(echo "$SSH_PRIVATE_KEY_BATH_OPENFLEXURE_BASE64" | base64 --decode)
    - mkdir -p ~/.ssh
    - echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
    
  script:
    # Install rsync if not already installed
    - 'which rsync || ( apt-get update -y && apt-get install rsync -y )'
    
    # Upload the builds folder to openflexure-microscope builds 
    - rsync -hrvz -e ssh dist_electron/ ci-user@openflexure.bath.ac.uk:/var/www/build/openflexure-ev/
    
    # Run update-latest.py on the build server
    - ssh -t ci-user@openflexure.bath.ac.uk "/var/www/build/update-latest.py"
    
  only:
    - /^v(\d+\.)*\d+$/  # Stable versions (e.g. no prerelease suffix)
    - web  # Allow force publish via web

# Snap release
release-snap:
  image: snapcore/snapcraft:stable
  stage: release
  dependencies:
    - build

  before_script: []
  script:
    # Publish snap package
    - echo "Publishing snap package..."
    - echo "$SNAPCRAFT_LOGIN" | base64 -d > snapcraft.login
    - snapcraft login --with snapcraft.login
    - for f in dist_electron/*.snap; do snapcraft upload --release=stable $f; done
  only:
    - /^v(\d+\.)*\d+$/  # Stable versions (e.g. no prerelease suffix)
    - web  # Allow force publish via web

# Chocolatey release
release-choco:
  image: 
    name: linuturk/mono-choco
    entrypoint: [""]
  stage: release
  dependencies:
    - build

  script:
    - chmod +x ./choco/make-nupkg.sh
    - ./choco/make-nupkg.sh
    - cd ./choco/release
    - choco pack  --allow-unofficial
    - choco apikey --allow-unofficial --key "$CHOCO_APIKEY" --source https://push.chocolatey.org/
    - choco push --allow-unofficial --source https://push.chocolatey.org/

  only:
    - /^v(\d+\.)*\d+$/  # Stable versions (e.g. no prerelease suffix)
    - web  # Allow force publish via web
